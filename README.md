# Installation
      
## 1. Please make sure you install the SDK package from the Official Leap Motion Website, [here](https://developer.leapmotion.com/). 

## 2. Also, make sure that you have added the /lib folder to your PYTHONPATH environmental variable. 
                    
* Mac:
    * echo export 'PYTHONPATH="${PYTHONPATH}:<<place the dir of the lib folder here and take the pointy brackets away>>"' >> ~/.bash_profile
        * eg.echo export 'PYTHONPATH="${PYTHONPATH}:/Users/KangShiang/LeapSDK/lib"' >> ~/.bash_profile
* Linux:
    * echo export 'PYTHONPATH="${PYTHONPATH}:<<place the dir of the lib folder here and take the pointy brackets away>>"' >> ~/.bashrc 
        * eg.echo export 'PYTHONPATH="${PYTHONPATH}:/Users/KangShiang/LeapSDK/lib"' >> ~/.bashrc

## 3. Clone the repository:
    * 1. "cd" into the directory you would like to place your local repository. 
    * 2. type "git clone https://ykshiang@bitbucket.org/ykshiang/capstone-pi.git"
    * 3. "cd capstone-pi"
    * 4. "python main.py"

# Dependencies
                      
## 1. Please make sure that you have all the dependencies installed on your computer. Eventually, virtual environment will be used to make deployment easier. However, at this point of the project, we are installing library directly into the python environmental library folder. So, please make sure that you have these dependencies installed on your computer in order to run the program on your computer. 

    * sudo pip install netifaces

# Setup 

## Please setup the project by doing as follows:
* chmod +x run


# To Run the project

## Simple execute the "./run" script by typing "./run" on the command line
