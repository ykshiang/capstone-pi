# Echo server program
import socket
#import netifaces as neti
import struct
import thread
import time

class Conn:
    # Declaring Variable for Conn
    enName = ''
    HOST = '0.0.0.0'    # Using 0.0.0.0 to accept connection from anywhere.
    PORT = 1078         # TODO: using 1078 for now, but need to RESEARCH if 1078 is already used for other processes.
    count = 0
    WEASIS_IP = ''

    # Init method of Conn. Basically it sets up the socket listening to the assigned.
    def __init__(self):
        thread.start_new_thread(self.setup_multicast_server, ())

    def setup_tcp_client(self):
        for i in range(5):
            time.sleep(1)
            print(".")
        print("Connecting to Weasis")
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn.connect((self.WEASIS_IP, self.PORT))

    def setup_multicast_server(self):
        print("Setting up autoconnect signal receiver")
        MCAST_GRP = '224.1.1.1'
        MCAST_PORT = 5007
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # use MCAST_GRP instead of '' to listen only
        # to MCAST_GRP, not all groups on MCAST_PORT
        sock.bind(('', MCAST_PORT))
        mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        while True:
            self.WEASIS_IP = sock.recv(10240)
            print("Receiving autoconnect signal...")
            print("Weasis's IP is " + self.WEASIS_IP)
            self.setup_tcp_client()
            break


    # On receive method handler.
    def on_receive_data(self, data):
        return

    # The method used to send data.
    def send_data(self, data):
        if self.count > 0:
            try:
                self.conn.sendall(data)
                print(data)
            except:
                return
            self.count = 0
        else:
            self.count += 1