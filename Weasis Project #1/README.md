**Installing Eclipse for Weasis**
---------------------------------

*Install the latest JDK for your system*

[Click here for
JDK](<http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>)

**Install Eclipse for your system**

[Click here for Eclipse](<https://eclipse.org/>)

On the installer, select Eclipse for Java, you will automatically download GIT
and Mavens which are critical.

**Run Eclipse**

Create a Project

In *Window \> Preferences \> General \> Workspace*

-   Select UTF-8 for *Text file encoding*

    -   Select Unix for *New text file line delimiter*

    In *Window \> Preferences \> Java Installed JREs*

    -   Uncheck any JRE’s

    -   Add your installed JDK

    In *Window \> Perspective \> Open Perspective \> Other... \> Git*

    -   In the new Git Repositories Window click Clone Repository

        add **git://github.com/nroduit/Weasis.git**

    Click on *File \> Import*

    -   Click on *Maven \> Existing Maven Projects*

        -   Select *Weasis*

    In Package Explorer expand weasis-framework

    -   Right Click *pom.xml \> Run As \> Maven Install*

    -   Verify that the installation succeeded in your console.

    In Package Explorer Right-Click Weasis-launcher

    -   *Debug as \> Debug Configurations*

        -   Under the *Main* tab in *Main Class* add
            *org.pushingpixels.lafwidget.ant.ContainerGhostingAugmenter*

        -   Under the *Arguements Tab* in *VM Arguements* add the following
            line, replacing "YourM2Repository" :

        *-Xms32m -Xmx512m -Dmaven.localRepository="YourM2Repository/arguements"
        -Dgosh.args="-sc telnetd -p 17179 start"*

    -   Try clicking DEBUG for the DICOM Viewer to Open.
