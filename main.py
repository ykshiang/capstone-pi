import os, sys, inspect, thread, time
import json
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))
import Leap

from conn import Conn
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

def toCustomStructure(obj):
    rslt = ""
    for key, value in obj.iteritems():
        rslt = ";" + str(key) + ":" + str(value) + rslt

    if rslt is "":
        return rslt
    else:
        return rslt[1:]

# sockConn is a global object. I am not sure if we should make it global, but what I am thinking is to make this
# accessible everywhere in this file.
# SockConn is a class inherited from Conn.
class SockConn(Conn):
    # This method is called everytime when a message is received from the Java Client.
    def on_receive_data(self, data):
        print("Received:")
        print data
# Create an object using SockConn defined above.
sockConn = SockConn()

# SampleListener is a class inherited from Leap Listener. Implementation of Leap Listener can be found in Leap.py
class SampleListener(Leap.Listener):
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    bone_names = ['Metacarpal', 'Proximal', 'Intermediate', 'Distal']
    state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']

    # This method is called every time a new Leap Motion is detected.
    def on_connect(self, controller):
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE)
        controller.config.set("Gesture.Circle.MinArc", 1)

    # This method is called everytime the program receives a frame update from the Leap Device.
    def on_frame(self, controller):
        frame = controller.frame()
        rslt = ""
        # Get gestures
        for gesture in frame.gestures():
            if gesture.type == Leap.Gesture.TYPE_CIRCLE:
                circle = CircleGesture(gesture)
                circle_radius = ( 'circleRadius:%s;' %str(circle.radius))
                # Determine clock direction using the angle between the pointable and the circle normal
                if circle.pointable.direction.angle_to(circle.normal) <= Leap.PI/2:
                    clockwiseness = "clockwise"
                else:
                    clockwiseness = "counterclockwise"
                rslt = (
                    "handDetected:%s;" % "True" +
                    "motionType:%s;" % "circular" +
                    "clockwiseness:%s;" % clockwiseness +
                    circle_radius
                )
            elif gesture.type == Leap.Gesture.TYPE_SWIPE:
                swipe = SwipeGesture(gesture)
                rslt = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % "swipe" +
                    "swipeID:%s;" % str(gesture.id) +
                    "state:%s;" % str(self.state_names[gesture.state]) +
                    "positionX:%s;" % str(swipe.position.x) +
                    'positionY:%s;' % str(swipe.position.y) +
                    'positionZ:%s;' % str(swipe.position.z) +
                    'directionX:%s;' % str(swipe.direction.x) +
                    'directionY:%s;' % str(swipe.direction.y) +
                    'directionZ:%s;' % str(swipe.direction.z) +
                    'speed:%s;' % str(swipe.speed)
                    )

        for hand in frame.hands:
            if len(frame.fingers.extended()) is 1:
                position1=hand.palm_position
                fingerdata_1 = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % '1fingers' +
                    'positionX:%s;' % str(position1.x) +
                    'positionY:%s;' % str(position1.y) +
                    'positionZ:%s;' % str(position1.z)
                )
                rslt = rslt + fingerdata_1

            elif len(frame.fingers.extended()) is 2:
                position2=hand.palm_position
                fingerdata_2 = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % '2fingers' +
                    'positionX:%s;' % str(position2.x) +
                    'positionY:%s;' % str(position2.y) +
                    'positionZ:%s;' % str(position2.z)
                )
                rslt = rslt + fingerdata_2

            elif len(frame.fingers.extended()) is 3:
                position3=hand.palm_position
                fingerdata_3 = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % '3fingers' +
                    'positionX:%s;' % str(position3.x) +
                    'positionY:%s;' % str(position3.y) +
                    'positionZ:%s;' % str(position3.z)
                )
                rslt = rslt + fingerdata_3

            elif len(frame.fingers.extended()) is 4:
                position4=hand.palm_position
                fingerdata_4 = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % '4fingers' +
                    'positionX:%s;' % str(position4.x) +
                    'positionY:%s;' % str(position4.y) +
                    'positionZ:%s;' % str(position4.z)
                )
                rslt = rslt + fingerdata_4

            elif len(frame.fingers.extended()) is 5:
                position5=hand.palm_position
                fingerdata_5 = (
                    "handDetected:%s;" % "True" +
                    'motionType:%s;' % '5fingers' +
                    'positionX:%s;' % str(position5.x) +
                    'positionY:%s;' % str(position5.y) +
                    'positionZ:%s;' % str(position5.z)
                )
                rslt = rslt + fingerdata_5

        if rslt != "":
            # print(rslt)
            sockConn.send_data(rslt)
        else:
            sockConn.send_data("error:motionUnrecognized;")

def main():
    # Create a Leap listener object.
    listener = SampleListener()

    # Create a Leap controller object.
    controller = Leap.Controller()

    # Add the Leap listener object to the Leap controller.
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        controller.remove_listener(listener)

if __name__ == "__main__":
    main()
