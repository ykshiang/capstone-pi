

# Server program

#import socket

# Set the socket parameters
#addr = ('', 33333)  # host, port

# Create socket and bind to address
#UDPSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#UDPSock.bind(addr)

# Receive messages
#while True:
 #   data, addr = UDPSock.recvfrom(1024)
  #  if data == 'stop':
   #     print 'Client wants me to stop.'
    #    break
    #else:
    #    print "From addr: '%s', msg: '%s'" % (addr[0], data)

# Close socket
#UDPSock.close()
#print 'Server stopped.'

#---------------------------------------------------------------

# Client program

#import socket

from socket import *

myip = gethostbyname(gethostname())
print 'My IP',myip

# XX: assumes /24 address
broadip = inet_ntoa( inet_aton(myip)[:3] + b'\xff' )
print 'LAN broadcast', broadip
#addr = ('localhost', 33333)                                # localhost, port
#addr = ('127.0.0.1', 33333)                                # localhost explicitly
#addr = ('xyz', 33333)                                      # explicit computer
#addr = ('<broadcast>', 33333)                              # broadcast address
addr = (broadip, 33333)                          # broadcast address explicitly

UDPSock = socket(AF_INET,SOCK_DGRAM) # Create socket

print 'Enter your message:'
print '- Empty message to stop this client.'
print '- "stop" to stop all servers.'

# Almost infinite loop... ;)
while True:
    data = raw_input('>> ')
    if len(data) == 0:
        break
    else:
        if UDPSock.sendto(data, addr):
            print "Sending message '%s'..." % data

UDPSock.close()             # Close socket
print 'Client stopped.'